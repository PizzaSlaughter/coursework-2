#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

#Import libraries
import numpy as np
import rospy  
#Jointstates describes the states of the robot's joints eg.velocity,pos,effect
from sensor_msgs.msg import JointState 
#Header publishes messages 
from std_msgs.msg import Header


def talker():
#initialise class publisher 
    #declare node in topic joint_state
    pub = rospy.Publisher('joint_states', JointState, queue_size=10)
    #initialise comunication with ROS Master
    rospy.init_node('ambra_joint_state_publisher')
    #set processing time
    rate = rospy.Rate(10) # 10hz

    #initialise class joint:
    #declare joint instance	
    joint = JointState()
    #create header
    joint.header = Header() 
    #ensure that the subscriber takes only the last message from the publisher 
    joint.header.stamp = rospy.Time.now() 
    
    #loop as long as the terminal is on
    while not rospy.is_shutdown(): 
	#Initialise list of joints
	joint.name = ['base_link_screw', 'screw_holder','holder_stick']
	coord = np.zeros(3) #preallocate coordinates
	
        #ask position for each joint
	for j, jointname in enumerate(joint.name):
		#receive user input and convertstring to number
		print "User input for joint {}:".format(j+1)
		inputs = raw_input()
		coord[j] = float(inputs)
		#ensure within limits for joint base_link_screw
		if (j)==0:
			while coord[j] < -3.14 or coord[j] > 3.14:
				print "Error, beyond {} joint limit, must be between -3.14 and 3.14. Please enter again:".format(jointname)
				#ask user new input
				inputs = raw_input()
				coord[j] = float(inputs)
		#ensure within limits for joints screw_holder, holder_stick
		if (j)==2 or (j)==1:
			while coord[j] < -1.57 or coord[j] > 1.57:
				print "Error, beyond joint limit, must be between -1.57 and 1.57. Please enter again:"
				#ask user new input
				inputs = raw_input()
				coord[j] = float(inputs)

	#assign position to joints
	joint.position = coord
	joint.header.stamp =rospy.Time.now()
	#publish new position
	pub.publish(joint)
	#maintain loop rate
	rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
